import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'sweet-form';

  /**
   *  @param form importado do @angular/forms
   * Lembrar também de importar também no app.module os
   * módulos FormsModule e ReactiveFormsModule
   */
  form: FormGroup;

  /**
   *
   * @param fb: Form Builder, ajuda na construção de formulários complexos
   * como formulários aninhados. Tipo aquele do flor do café.
   * Usei aqui porque gosto dele, dá pra fazer validações.
   */
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    // chama o método pra configurar o form
    this.buildForm();
  }

  /**
   * this.form é minhs instancia de formulário.
   * Estou informando que ele é um grupo composto por um atributo name,
   * esse atributo inicia vazio '' e tem uma validação de obrigatoriedade.
   * Não consegui desativar o botão porque é do Sweet alert. Mas se tivesse
   * um botao de submit próprio daria certo. É basicamente a mesma lógica do 
   * texto que coloquei abaixo do input de nome.
  */
  private buildForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
    });
  }

  /**
   * Esse método deveria estar na diretiva (ngSubmit)="onFormSubmit", como atributo
   * do <form>. Entretanto, como o botão de submissão usado será o do SWAL, coloquei para
   * quando confirmar o botao (atributo confirmButton="method()"), executar esse método.
  */
  onFormSubmit() {
    // Pega os valores do formulário e joga como objeto
    const values = this.form.value;

    // Aqui tu tem o atributo name preenchido lá do form. Dá pra trabalhar com ele como quiser
    console.log(values.name);
    console.log('clicou');
    this.form.reset();
    /** como não é uma submissão do formulário, ele nao é resetado automaticamente
     * Daí eu chamo a função de reset aqui pra limpar os campos.
    */
  }
}
